import json
import logging

import requests

from framework.proxy.sequence_picker import SequencePicker


class ProxyProvider:
    def __init__(self, proxy_picker = None):
        self.proxies = []
        self.proxy_picker = proxy_picker or SequencePicker()

    def load(self, url):
        logging.debug("Loading proxies from %s", url)
        self.proxies = json.loads(requests.get(url).text)
        logging.debug("Proxies loaded")

    @property
    def proxy_count(self):
        return len(self.proxies)

    def pick(self):
        return self.proxy_picker.pick(self.proxies)
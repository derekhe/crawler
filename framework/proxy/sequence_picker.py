from framework.proxy.proxy import Proxy


class SequencePicker:
    def __init__(self):
        self.index = 0

    def pick(self, proxies):
        proxy = Proxy(proxies[self.index])
        self.index += 1
        self.index %= len(proxies)

        return proxy

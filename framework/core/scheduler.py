#!/usr/bin/env python
import logging
import pickle
import time
from threading import Thread, RLock

import arrow
import pika

from framework.core.constant import Status, QueueNames
from framework.core.job import Job


class Scheduler:
    def __init__(self, max_jobs=10, max_retries=10, job_timeout=60):
        self.connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
        self.channel = self.connection.channel()
        self.channel.queue_declare(queue=QueueNames.JOB)
        self.channel.queue_declare(queue=QueueNames.JOB_STATUS)
        self.max_jobs = max_jobs
        self.max_retries = max_retries
        self.job_timeout = job_timeout
        self.jobs = {}
        self.jobs_lock = RLock()

    def generate(self):
        for content in self.job_generator():
            self.send(Job(
                content=content,
                status=Status.SENT
            ))

    def job_generator(self):
        pass

    def send(self, job):
        while self.jobs_count >= self.max_jobs:
            time.sleep(0.1)

        with self.jobs_lock:
            job.status = Status.SENT
            job.error = ''
            self.jobs[job.id] = job

            logging.debug('Sending %s', job)
            self.channel.basic_publish(exchange='',
                                       routing_key=QueueNames.JOB,
                                       body=pickle.dumps(job))

    def update_job(self, ch=None, method=None, properties=None, body=None):
        job = pickle.loads(body)
        logging.debug("Update job %s", body)
        with self.jobs_lock:
            self.jobs[job.id] = job

    def job_status_event_thread(self):
        self.channel.basic_consume(self.update_job,
                                   queue=QueueNames.JOB_STATUS,
                                   no_ack=True)

        self.channel.start_consuming()

    def job_status_update_thread(self):
        while True:
            time.sleep(0.1)
            self.update_jobs()

    def update_jobs(self):
        with self.jobs_lock:
            for k in list(self.jobs):
                job = self.jobs[k]
                self.update_jobs_by_status(job, k)
            logging.debug("Jobs count %s", self.jobs_count)

    def update_jobs_by_status(self, job, k):
        status = job.status
        if status == Status.FINISHED:
            self.jobs.pop(k)
        elif status == Status.CRAWL_FAILED:
            self.jobs.pop(k)
            if job.retries < self.max_retries:
                logging.debug("Job crawl failed, retry. Job: %s" % job)
                job.retries += 1
                self.send(job)
            else:
                logging.debug("Reach max retries Job: %s" % job)

        if self.is_timeout(job):
            logging.debug("Job timeout %s" % job)
            self.jobs.pop(k)

    def is_timeout(self, job):
        return arrow.get().timestamp - job.created >= self.job_timeout

    @property
    def jobs_count(self):
        return len(self.jobs)

    def clear_jobs(self):
        self.jobs.clear()

    def start(self):
        generator_thread = Thread(target=self.generate)
        generator_thread.start()

        job_status_event_thread = Thread(target=self.job_status_event_thread)
        job_status_event_thread.start()

        job_status_update_thread = Thread(target=self.job_status_update_thread)
        job_status_update_thread.start()

        generator_thread.join()

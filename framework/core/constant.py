from enum import Enum, unique, auto


@unique
class Status(Enum):
    UNKNOWN = auto()
    SENT = auto()
    FINISHED = auto()
    RETRY = auto()
    CRAWLED = auto()
    CRAWL_FAILED = auto()

class QueueNames:
    JOB_STATUS = 'job_status'
    JOB = 'job'
    STORAGE = 'storage'
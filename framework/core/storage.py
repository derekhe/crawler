#!/usr/bin/env python
import json
import logging
import pickle

import pika

from framework.core.constant import QueueNames


class Storage:
    def __init__(self):
        self.connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
        self.channel = self.connection.channel()
        self.channel.queue_declare(queue=QueueNames.STORAGE)

    def save(self, job, content):
        pass

    def callback(self, ch, method, properties, body):
        job = pickle.loads(body)

        self.save(job, job.crawl_status)

    def start(self):
        self.channel.basic_consume(self.callback,
                                   queue=QueueNames.STORAGE,
                                   no_ack=True)

        logging.debug("Storage waiting for jobs")
        self.channel.start_consuming()

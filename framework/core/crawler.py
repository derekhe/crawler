#!/usr/bin/env python
import logging
import pickle
from concurrent.futures import ThreadPoolExecutor

import pika

from framework.core.constant import QueueNames, Status


class Crawler:
    def __init__(self, max_workers=10):
        self.connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
        self.channel = self.connection.channel()
        self.channel.queue_declare(queue=QueueNames.JOB)
        self.channel.queue_declare(queue=QueueNames.STORAGE)
        self.executor = ThreadPoolExecutor(max_workers=max_workers)

    def send_to_storage(self, job, crawl_result):
        job.status = Status.CRAWLED
        job.crawl_status.result = crawl_result
        body = pickle.dumps(job)
        logging.debug(body)
        self.channel.basic_publish(exchange='',
                                   routing_key=QueueNames.STORAGE,
                                   body=body)

    def job_finished(self, job):
        job.status = Status.FINISHED
        body = pickle.dumps(job)
        logging.debug(body)
        self.channel.basic_publish(exchange='',
                                   routing_key=QueueNames.JOB_STATUS,
                                   body=body)

    def crawl_error(self, job):
        job.status = Status.CRAWL_FAILED
        body = pickle.dumps(job)
        logging.debug(body)
        self.channel.basic_publish(exchange='',
                                   routing_key=QueueNames.JOB_STATUS,
                                   body=body)

    def crawl(self, content):
        pass

    def crawl_thread(self, job):
        try:
            result = self.crawl(job.content)
            if result == None:
                self.job_finished(job)
            else:
                self.send_to_storage(job, result)
        except Exception as ex:
            logging.exception(ex)
            job.crawl_status.error = str(ex.__class__.__name__)
            self.crawl_error(job)

    def job_received(self, ch, method, properties, body):
        job = pickle.loads(body)
        logging.debug("received %s", job)
        self.executor.submit(self.crawl_thread, job)

    def start(self):
        self.channel.basic_consume(self.job_received,
                                   queue=QueueNames.JOB,
                                   no_ack=True)

        self.channel.start_consuming()

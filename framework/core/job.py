import uuid

import arrow

from framework.core.constant import Status


class CrawlStatus:
    def __init__(self):
        self.result = None
        self.error = None


class Job:
    def __init__(self, id=None, content=None, status=Status.UNKNOWN, retries = 0, created = arrow.get().timestamp):
        self.crawl_status = CrawlStatus()
        self.id = id or str(uuid.uuid4())
        self.content = content
        self.status = status
        self.created = created or arrow.get().timestamp
        self.retries = retries

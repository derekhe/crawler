import json
import pickle
import unittest
from unittest.mock import MagicMock, patch, ANY

from framework.core.constant import Status, QueueNames
from framework.core.crawler import Crawler
from framework.core.job import Job


class TestCrawler(unittest.TestCase):
    def setUp(self):
        self.mock_pika = patch("framework.core.crawler.pika").start()
        self.mock_connection = MagicMock()
        self.mock_pika.BlockingConnection.return_value = self.mock_connection
        self.mock_channel = MagicMock()
        self.mock_connection.channel.return_value = self.mock_channel

    def test_should_send_to_storage_when_job_is_crawled(self):
        class DummyCrawler(Crawler):
            def crawl(self, content):
                return 'crawled'

        job = Job('my content', Status.SENT)

        crawler = DummyCrawler()

        crawler.job_received(None, None, None, body=pickle.dumps(job))
        crawler.executor.shutdown()

        job.status = Status.CRAWLED
        job.crawl_status.result = 'crawled'

        body = pickle.dumps(job)

        self.mock_channel.basic_publish.assert_called_with(body=body, exchange=ANY, routing_key=QueueNames.STORAGE)

    def test_should_report_crawl_failure_when_job_is_failed(self):
        class DummyCrawler(Crawler):
            def crawl(self, content):
                raise TimeoutError

        job = Job('my content', Status.SENT)

        crawler = DummyCrawler()

        crawler.job_received(None, None, None, body=pickle.dumps(job))
        crawler.executor.shutdown()

        job.status = Status.CRAWL_FAILED
        job.crawl_status.error = "TimeoutError"
        body = pickle.dumps(job)

        self.mock_channel.basic_publish.assert_called_with(body=body, exchange=ANY, routing_key=QueueNames.JOB_STATUS)

    def test_should_report_finished_when_job_get_nothing(self):
        class DummyCrawler(Crawler):
            def crawl(self, content):
                return None

        job = Job('my content', Status.SENT)

        crawler = DummyCrawler()

        crawler.job_received(None, None, None, body=pickle.dumps(job))
        crawler.executor.shutdown()

        job.status = Status.FINISHED
        body = pickle.dumps(job)

        self.mock_channel.basic_publish.assert_called_with(body=body, exchange=ANY, routing_key=QueueNames.JOB_STATUS)

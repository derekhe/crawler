import pickle
import unittest
import uuid
from threading import Thread
from unittest import mock
from unittest.mock import MagicMock, patch, PropertyMock

import arrow

from framework.core.constant import Status
from framework.core.job import Job
from framework.core.scheduler import Scheduler


class TestScheduler(unittest.TestCase):
    def setUp(self):
        self.mock_pika = patch("framework.core.scheduler.pika").start()
        self.mock_connection = MagicMock()
        self.mock_pika.BlockingConnection.return_value = self.mock_connection
        self.mock_channel = MagicMock()
        self.mock_connection.channel.return_value = self.mock_channel
        self.scheduler = Scheduler()

    def test_should_generate_jobs_in_queue_and_send(self):
        count = 10

        def my_generator():
            for i in range(0, count):
                yield i

        scheduler = self.scheduler
        scheduler.job_generator = my_generator
        scheduler.generate()

        self.assertEqual(scheduler.jobs_count, count)

    def test_should_pause_send_when_jobs_are_full(self):
        scheduler = Scheduler(max_jobs=1)

        scheduler.send(self._generate_job('content'))
        thread = Thread(target=scheduler.send, args=(self._generate_job('another content'),))
        thread.start()

        # Now it should have only 1 job in queue, send thread is waiting
        self.assertEqual(scheduler.jobs_count, 1)

        # Now it should have 0 job in queue
        scheduler.clear_jobs()

        thread.join(timeout=1)
        self.assertFalse(thread.is_alive())

        # When jobs are empty, it will send another one
        self.assertEqual(scheduler.jobs_count, 1)

    def test_should_not_remove_job_when_status_is_sent(self):
        scheduler = self.scheduler
        scheduler.send(self._generate_job('job1'))
        job1 = self._get_first_job(scheduler)
        self.assertEqual(job1.status, Status.SENT)

        scheduler.update_job(body=pickle.dumps(job1))
        scheduler.update_jobs()

        self.assertEqual(scheduler.jobs_count, 1)

    def test_should_remove_job_when_status_is_finished(self):
        scheduler = self.scheduler
        scheduler.send(self._generate_job('job1'))
        job1 = self._get_first_job(scheduler)
        self.assertEqual(job1.status, Status.SENT)

        job1.status = Status.FINISHED
        scheduler.update_job(body=pickle.dumps(job1))
        scheduler.update_jobs()

        self.assertEqual(scheduler.jobs_count, 0)

    def test_should_send_job_again_when_crawl_failed(self):
        scheduler = self.scheduler
        job = self._generate_job('job1')
        job.crawl_status.error = 'TimeoutError'
        scheduler.send(job)

        job1 = self._get_first_job(scheduler)
        self.assertEqual(job1.status, Status.SENT)

        job1.status = Status.CRAWL_FAILED
        scheduler.update_job(body=pickle.dumps(job1))
        scheduler.update_jobs()

        retried_job = self._get_first_job(scheduler)
        self.assertEqual(scheduler.jobs_count, 1)
        self.assertEqual(retried_job.id, job.id)
        self.assertEqual(retried_job.status, Status.SENT)
        self.assertEqual(retried_job.error, '')

    def test_should_not_send_job_again_when_reach_max_retries(self):
        scheduler = Scheduler(max_retries=2)

        scheduler.jobs = {
            '1': Job("1", 'content 1', retries=1, status=Status.CRAWL_FAILED),
            '2': Job("2", 'content 2', retries=2, status=Status.CRAWL_FAILED)
        }
        scheduler.update_jobs()

        retried_job = self._get_first_job(scheduler)
        self.assertEqual(scheduler.jobs_count, 1)
        self.assertEqual(retried_job.status, Status.SENT)
        self.assertEqual(retried_job.retries, 2)

    @mock.patch('framework.core.scheduler.arrow')
    def test_should_remove_job_when_reach_timeout_limit(self, mock_arrow):
        arrow_get = MagicMock()
        mock_timestamp = arrow.get().timestamp
        type(arrow_get).timestamp = PropertyMock(return_value=mock_timestamp)
        mock_arrow.get.return_value = arrow_get

        scheduler = Scheduler(job_timeout=1)

        scheduler.jobs = {
            '1': Job("1", 'content 1', retries=1, status=Status.CRAWL_FAILED, created=mock_timestamp),
            '2': Job("2", 'content 2', retries=2, status=Status.CRAWL_FAILED, created=mock_timestamp - 2)
        }

        scheduler.update_jobs()
        self.assertEqual(scheduler.jobs_count, 1)
        first_job = self._get_first_job(scheduler)
        self.assertEqual(first_job.status, Status.SENT)
        self.assertEqual(first_job.id, '1')

        type(arrow_get).timestamp = PropertyMock(return_value=mock_timestamp + 1)

        scheduler.update_jobs()
        self.assertEqual(scheduler.jobs_count, 0)

    def _get_first_job(self, scheduler):
        k1 = list(scheduler.jobs)[0]
        return scheduler.jobs[k1]

    def _generate_job(self, content):
        id = str(uuid.uuid4())

        return Job(id, content, Status.SENT)

import json
import unittest

import requests_mock
from framework.proxy.proxy_provider import ProxyProvider


class TestProxyProvider(unittest.TestCase):
    def setUp(self):
        self.provider = ProxyProvider()

        with requests_mock.mock() as m:
            m.get('http://proxyurl', text=json.dumps(['http://proxy1', 'http://proxy2']))
            self.provider.load('http://proxyurl')

    def test_should_load_proxy_from_url(self):
        self.assertEqual(self.provider.proxy_count, 2)

    def test_should_pick_proxy_one_by_one_by_default(self):
        self.assertEqual(self.provider.pick().url, 'http://proxy1')
        self.assertEqual(self.provider.pick().url, 'http://proxy2')
        self.assertEqual(self.provider.pick().url, 'http://proxy1')
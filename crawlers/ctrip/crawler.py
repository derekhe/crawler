import datetime
import logging
import logging.config

import arrow
import requests
import ujson as json

from framework.core.crawler import Crawler
from framework.proxy.proxy_provider import ProxyProvider


class CtripCrawler(Crawler):
    def __init__(self):
        super().__init__()
        self.proxy_provider = ProxyProvider()
        self.proxy_provider.load("https://jsonblob.com/api/a9004050-f383-11e6-901d-352c2a36b464")

    def crawl(self, content):
        depart_date = arrow.get(content['depart']['date'])

        timestamp = int(datetime.datetime(depart_date.year, depart_date.month, depart_date.day).timestamp() * 1000)

        url = "https://sec-m.ctrip.com/restapi/soa2/11781/Domestic/FlightList/Query"

        payload = '{"preprdid":"","trptpe":"1","flag":8,"seat":0,"extendinfo":[{"extype":5,"expam":1}],"searchitem":[{"dccode":"%s","accode":"%s","dtime":"%s"}],"sflgno":"","head":{"cid":"%s","ctok":"","cver":"1.0","lang":"01","sid":"8888","syscode":"09","auth":null,"extension":[{"name":"protocal","value":"http"}]},"contentType":"json"}' % (
            content['depart']['city']['code'], content['arrive']['city']['code'], content['depart']['date'],
            9031092411392651247 + timestamp)

        headers = {
            'accept': "application/json",
            'cookieorigin': "http://wap.ctrip.com",
            'origin': "http://wap.ctrip.com",
            'user-agent': "Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS X) AppleWebKit/601.1.46 (KHTML, like Gecko) Version/9.0 Mobile/13B143 Safari/601.1",
            'content-type': "application/json",
            'referer': "http://wap.ctrip.com/html5/flight/flight-list.html?triptype=1&dcode=SHA&acode=BJS&ddate=2017-03-13&seo=0&dfilter=sort%3Aprice%2Casc",
            'accept-encoding': "gzip, deflate, br",
            'accept-language': "en-US,en;q=0.8,zh-CN;q=0.6,zh;q=0.4,ja;q=0.2",
            'cache-control': "no-cache",
        }

        proxy = self.proxy_provider.pick()
        logging.debug("Using proxy %s", proxy.url)
        response = requests.request("POST", url, data=payload, headers=headers,
                                    proxies={"https": proxy.url},
                                    timeout=4)

        return self.parse(response.text)

    def parse(self, content):
        try:
            r = json.loads(content)
            if len(r["fltitem"]) != 0:
                return content
            else:
                logging.debug("Invalid content %s", content)
                return None
        except Exception as ex:
            logging.debug("Parse failed due to: %s", ex)
            raise ex

logging.config.fileConfig('./logconfig.ini')
CtripCrawler().start()

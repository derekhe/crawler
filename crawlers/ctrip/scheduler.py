import json
import logging.config

import arrow

from framework.core.scheduler import Scheduler


class CtripScheduler(Scheduler):
    def __init__(self):
        super().__init__()

        with open('./data/hotcities.json', encoding="utf-8") as data:
            self.cities = json.load(data)

        with open('./data/invalid-airline.txt') as data:
            self.invalid_airline = data.read().splitlines()

    def job_generator(self):
        for depart in self.cities:
            for arrive in self.cities:
                if depart["city"] == arrive["city"]:
                    continue

                for depart_port in depart["ports"]:
                    for arrive_port in arrive["ports"]:

                        if ("%s-%s" % (depart_port["code"], arrive_port["code"])) in self.invalid_airline:
                            continue

                        for d in range(0, 1):
                            depart_date = arrow.now("CST").shift(days=d)

                            yield {
                                'depart': {
                                    'city': {'name': depart['city'], 'code': depart['code']},
                                    'code': depart_port['code'],
                                    'date': depart_date.format("YYYY-MM-DD")
                                },
                                'arrive': {
                                    'city': {'name': arrive['city'], 'code': arrive['code']},
                                    'code': arrive_port['code']
                                }
                            }

logging.config.fileConfig('./logconfig.ini')
CtripScheduler().start()
import logging.config

from framework.core.scheduler import Scheduler


class SimpleScheduler(Scheduler):
    def __init__(self):
        super().__init__(job_timeout=3)

    def job_generator(self):
        for i in range(0,1):
            yield i

logging.config.fileConfig('./logconfig.ini')
SimpleScheduler().start()
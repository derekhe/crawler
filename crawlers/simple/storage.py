import logging.config

from framework.core.storage import Storage


class SimpleStorage(Storage):
    def __init__(self):
        super().__init__()

    def save(self, job, content):
        print(content)

logging.config.fileConfig('./logconfig.ini')
SimpleStorage().start()

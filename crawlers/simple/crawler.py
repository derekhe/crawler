import logging.config

from framework.core.crawler import Crawler


class SimpleCrawler(Crawler):
    def __init__(self):
        super().__init__()

    def crawl(self, content):
        return None

logging.config.fileConfig('./logconfig.ini')
SimpleCrawler().start()
